# Zig Language highlighting for Geany
This gives you a basic working syntax highlighting for the Geany editor. To
install, copy ```filetypes.zig.conf``` into ```~/.config/geany/filedefs``` and
edit ```/.config/geany/filetype_extensions.config```, adding this line in the
```[Extensions]``` section:
```
Zig=*.zig;
```
## Limitations
This will provide basic syntax highlighting *only*. There is currently no symbol
lookup. I am working on my own fork of Geany that could provide symbol lookup,
but it requires wrok that would need to be upstreamed to two other projects.
* [Scintilla](https://www.scintilla.org/) - provides the syntax highlighting for
geany. This package re-uses the Rust scintilla lexer, which I have already forked
and partially modified for Zig.
* [Universal Ctags](https://github.com/universal-ctags/ctags) - provides tag and
symbol lookup for Geany. A second Zig lexer would need to be implemented for this
project, as lexing for syntax and lexing for tags are handled by two different
upstream codebases in Geany.

Additionally, as we are re-using the Rust lexer for highlighting right now, there
will be certain Zig specific constructs that are missed. What is here is usable
now as-is, however.
